import React, { Component } from "react";

class CalculatorOutput extends Component {
  buttonPressed = (e) => {
    this.props.buttonPressed(e.target.name);
  };
  render() {
    return (
      <div>
        <table>
          <tr>
            <td colSpan="4">
              <input type="text"></input>
            </td>
          </tr>
          <tr>
            <td>
              <button name="AC" data-action="clear">
                AC
              </button>
            </td>
            <td>
              <button name="+/-">+/-</button>
            </td>
            <td>
              <button name="%" onClick={this.buttonPressed}>
                %
              </button>
            </td>
            <td>
              <button name="/" onClick={this.buttonPressed}>
                /
              </button>
            </td>
          </tr>
          {/*row one*/}
          <tr>
            <td>
              <button name="1" onClick={this.buttonPressed}>
                1
              </button>
            </td>
            <td>
              <button name="2" onClick={this.buttonPressed}>
                2
              </button>
            </td>
            <td>
              <button name="3" onClick={this.buttonPressed}>
                3
              </button>
            </td>
            <td>
              <button name="*" onClick={this.buttonPressed}>
                x
              </button>
            </td>
          </tr>
          {/*row two*/}
          <tr>
            <td>
              <button name="4" onClick={this.buttonPressed}>
                4
              </button>
            </td>
            <td>
              <button name="5" onClick={this.buttonPressed}>
                5
              </button>
            </td>
            <td>
              <button name="6" onClick={this.buttonPressed}>
                6
              </button>
            </td>
            <td>
              <button name="-" onClick={this.buttonPressed}>
                -
              </button>
            </td>
          </tr>
          {/*row three*/}
          <tr>
            <td>
              <button name="7" onClick={this.buttonPressed}>
                7
              </button>
            </td>
            <td>
              <button name="8" onClick={this.buttonPressed}>
                8
              </button>
            </td>
            <td>
              <button name="9" onClick={this.buttonPressed}>
                9
              </button>
            </td>
            <td>
              <button name="+" onClick={this.buttonPressed}>
                +
              </button>
            </td>
          </tr>
          {/*row four*/}
          <tr>
            <td>
              <button name="0" onClick={this.buttonPressed}>
                0
              </button>
            </td>
            <td>
              <button name="." onClick={this.buttonPressed}>
                .
              </button>
            </td>
            <td>
              <button name="=" onClick={this.buttonPressed}>
                =
              </button>
            </td>
          </tr>
        </table>
      </div>
    );
  }
}

export default CalculatorOutput;
